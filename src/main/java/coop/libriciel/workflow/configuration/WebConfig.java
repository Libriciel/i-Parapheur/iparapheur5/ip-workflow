/*
 * Workflow
 * Copyright (C) 2019-2024 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.configuration;

import coop.libriciel.workflow.services.FlowableService;
import coop.libriciel.workflow.utils.InstanceResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
@EnableSpringDataWebSupport
public class WebConfig implements WebMvcConfigurer {


    // <editor-fold desc="Beans">


    private final FlowableService flowableService;


    @Autowired
    public WebConfig(FlowableService flowableService) {
        this.flowableService = flowableService;
    }


    // </editor-fold desc="Beans">


    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new InstanceResolver(flowableService));
    }


}
