/*
 * Workflow
 * Copyright (C) 2019-2024 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.flowable.engine.repository.ProcessDefinition;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static coop.libriciel.workflow.utils.TextUtils.I_PARAPHEUR_INTERNAL;

@Log4j2
@Data
@NoArgsConstructor
public class WorkflowDefinition {


    public static final String API_DOC_ID_VALUE = "Workflow definition Id";
    public static final String API_ID_PATH = "definitionId";
    public static final String API_DOC_KEY_VALUE = "Workflow definition key";
    public static final String API_KEY_PATH = "definitionKey";

    public static String FINAL_DESK_ATTR_NAME = I_PARAPHEUR_INTERNAL + "final_desk_attr";
    public static String FINAL_NOTIFIED_DESKS_ATTR_NAME = I_PARAPHEUR_INTERNAL + "final_notified_desks_attr";


    @Schema(example = "example_workflow_01")
    private String id;

    @Schema(example = "example_workflow_01")
    private String key;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private int version;

    @Hidden
    @JsonIgnore
    private String deploymentId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY,
            description = """
                          We're assuming that we're counting every usage, past, pending or upcoming here.
                          Special case : If we're using a single workflow twice, as creation and validation,
                          it will count as 2 usages, even if there is only one folder.
                          """)
    private long usageCount;

    @Schema(example = "Example workflow")
    private String name;

    @Schema(example = """
                      [
                          { "type": "VISA", "validatingDeskIds": ["group_01_id"], "parallelType": "OR" },
                          { "type": "SIGNATURE", "validatingDeskIds": ["group_02_id"], "parallelType": "OR" }
                      ]
                      """)
    private List<StepDefinition> steps;

    private String finalDeskId;
    private List<String> finalNotifiedDeskIds;


    // <editor-fold desc="Constructors">


    public WorkflowDefinition(@NotNull ProcessDefinition processDefinition) {
        log.debug("Definition from ProcessDefinition:{}", processDefinition);

        id = processDefinition.getId();
        key = processDefinition.getKey();
        name = processDefinition.getName();
        version = processDefinition.getVersion();

        deploymentId = processDefinition.getDeploymentId();
        usageCount = -1;
        steps = null;
    }


    // </editor-fold desc="Constructors">

}
