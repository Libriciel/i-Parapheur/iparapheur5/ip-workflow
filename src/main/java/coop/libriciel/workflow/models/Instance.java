/*
 * Workflow
 * Copyright (C) 2019-2024 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.workflow.utils.TextUtils;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.jetbrains.annotations.NotNull;
import org.jooq.Record;

import java.util.*;

import static coop.libriciel.workflow.models.Visibility.CONFIDENTIAL;
import static coop.libriciel.workflow.services.DatabaseService.*;
import static coop.libriciel.workflow.utils.CollectionUtils.parseMetadata;
import static coop.libriciel.workflow.utils.CollectionUtils.popValue;
import static coop.libriciel.workflow.utils.TextUtils.I_PARAPHEUR_INTERNAL;
import static coop.libriciel.workflow.utils.TextUtils.WORKFLOW_INTERNAL;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

/**
 * Quick notes :
 * - Once the Instance is created, a DRAFT task is pending, waiting for a START.
 * - As long as it is not started, the user can still change the creation/validation metadata key.
 * - To list drafts, pending, rejected and finished folders, we query for Tasks, from the runtime engine.
 * - To list archive folders, we query for Instances, from the history engine.
 */
@Log4j2
@Data
@NoArgsConstructor
public class Instance {

    public static final String API_DOC_ID_VALUE = "Instance Id";
    public static final String API_PATH = "instanceId";

    public static final String FOLDER_DEPLOYMENT_KEY = "i_Parapheur_internal_folder";

    public static final String META_ORIGIN_GROUP_ID = WORKFLOW_INTERNAL + "origin_group_id";
    public static final String META_FINAL_GROUP_ID = WORKFLOW_INTERNAL + "final_group_id";
    public static final String META_FINAL_NOTIFIED_GROUPS = WORKFLOW_INTERNAL + "final_notified_groups";
    public static final String META_LEGACY_ID = "i_Parapheur_internal_legacy_id";
    public static final String META_VISIBILITY = "i_Parapheur_internal_visibility";
    public static final String META_CREATION_DATE = I_PARAPHEUR_INTERNAL + "creation_date";
    public static final String META_DUE_DATE = I_PARAPHEUR_INTERNAL + "due_date";
    public static final String META_INSTANCE_ID = WORKFLOW_INTERNAL + "instance_id";
    public static final String META_WORKFLOW_INDEX = WORKFLOW_INTERNAL + "workflow_index";
    public static final String META_LOOP_COUNT = WORKFLOW_INTERNAL + "loop_count";
    public static final String META_VALIDATION_START_DATE = WORKFLOW_INTERNAL + "validation_start_date";
    public static final String META_CREATION_WORKFLOW_ID = WORKFLOW_INTERNAL + "creation_workflow_id";
    public static final String META_VALIDATION_WORKFLOW_ID = WORKFLOW_INTERNAL + "validation_workflow_id";
    public static final String META_CANDIDATE_DELEGATE_GROUPS = WORKFLOW_INTERNAL + "delegate_candidate_groups";


    @Schema(accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private String id;

    private String name;
    private String legacyId;
    private String businessKey;
    private String tenantId;

    @Schema(accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private Date startTime;

    private Date dueDate;

    @Schema(accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private Date endTime;

    private Map<String, String> variables = new HashMap<>();

    @Schema(accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private Set<String> readByUserIds;

    @Schema(accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private State state;

    @Schema(accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private List<Task> taskList = new ArrayList<>();

    @Hidden
    @JsonIgnore
    private String deploymentKey = FOLDER_DEPLOYMENT_KEY;

    @Hidden
    @JsonIgnore
    private String deploymentId;

    @Schema(accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private String processDefinitionId;

    private String originGroup;
    private String originUser;
    private String finalGroup;
    private List<String> finalNotifiedGroups;

    private String creationWorkflowId;
    private String validationWorkflowId;

    private Visibility visibility;


    public Instance(@NotNull HistoricProcessInstance histProcInst) {

        id = histProcInst.getId();
        name = histProcInst.getName();
        businessKey = histProcInst.getBusinessKey();
        tenantId = histProcInst.getTenantId();
        startTime = histProcInst.getStartTime();
        endTime = histProcInst.getEndTime();
        taskList = emptyList();
        processDefinitionId = histProcInst.getProcessDefinitionId();

        variables = histProcInst.getProcessVariables()
                .entrySet()
                .stream()
                .collect(toMap(
                        Map.Entry::getKey,
                        entry -> String.valueOf(entry.getValue())
                ));

        this.initFieldsFromVariables();
    }


    public Instance(@NotNull ProcessInstance procInst) {

        id = procInst.getId();
        name = procInst.getName();
        businessKey = procInst.getBusinessKey();
        tenantId = procInst.getTenantId();
        startTime = procInst.getStartTime();
        taskList = emptyList();
        processDefinitionId = procInst.getProcessDefinitionId();

        variables = procInst.getProcessVariables()
                .entrySet()
                .stream()
                .collect(toMap(
                        Map.Entry::getKey,
                        entry -> String.valueOf(entry.getValue())
                ));

        this.initFieldsFromVariables();
    }


    public Instance(@NotNull Record recordRow) {

        name = recordRow.get(SQL_INSTANCE_NAME, String.class);
        id = recordRow.get(SQL_INSTANCE_ID, String.class);
        startTime = recordRow.get(SQL_BEGIN_DATE, Date.class);
        endTime = recordRow.get(SQL_END_DATE, Date.class);

        variables = parseMetadata(recordRow.get(SQL_VARIABLES, String[][].class));

        readByUserIds = Optional.ofNullable(recordRow.get(SQL_READ_BY, String[].class))
                .map(List::of)
                .orElse(emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty) // TODO : Filter that directly in the SQL
                .collect(toSet());

        this.initFieldsFromVariables();
    }


    private void initFieldsFromVariables() {
        Optional.ofNullable(popValue(variables, META_DUE_DATE))
                .map(TextUtils::fromIso8601String)
                .ifPresent(date -> dueDate = date);

        legacyId = popValue(variables, META_LEGACY_ID);
        originGroup = popValue(variables, META_ORIGIN_GROUP_ID);
        finalGroup = popValue(variables, META_FINAL_GROUP_ID);
        creationWorkflowId = popValue(variables, META_CREATION_WORKFLOW_ID);
        validationWorkflowId = popValue(variables, META_VALIDATION_WORKFLOW_ID);

        visibility = Optional.ofNullable(popValue(variables, META_VISIBILITY))
                .map(Visibility::valueOf)
                .orElse(CONFIDENTIAL);

        finalNotifiedGroups = Optional.ofNullable(popValue(variables, META_FINAL_NOTIFIED_GROUPS))
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());
    }


}
