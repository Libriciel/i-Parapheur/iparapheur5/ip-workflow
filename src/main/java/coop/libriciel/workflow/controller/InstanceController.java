/*
 * Workflow
 * Copyright (C) 2019-2024 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.models.*;
import coop.libriciel.workflow.models.requests.FilteringParameter;
import coop.libriciel.workflow.models.requests.InstanceSearchRequest;
import coop.libriciel.workflow.models.requests.SearchByMetadataRequest;
import coop.libriciel.workflow.services.DatabaseService;
import coop.libriciel.workflow.services.FlowableService;
import coop.libriciel.workflow.utils.ApiUtils;
import coop.libriciel.workflow.utils.CollectionUtils;
import coop.libriciel.workflow.utils.InstanceResolver.InstanceResolved;
import coop.libriciel.workflow.utils.PaginatedList;
import coop.libriciel.workflow.utils.WorkflowStepDefinitionAccessor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.runtime.ProcessInstanceQuery;
import org.jetbrains.annotations.NotNull;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URLDecoder;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;

import static coop.libriciel.workflow.models.Instance.*;
import static coop.libriciel.workflow.models.SortBy.Constants.INSTANCE_NAME_VALUE;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.Visibility.CONFIDENTIAL;
import static coop.libriciel.workflow.utils.ApiUtils.*;
import static coop.libriciel.workflow.utils.CollectionUtils.safeAdd;
import static coop.libriciel.workflow.utils.TextUtils.WORKFLOW_INTERNAL;
import static coop.libriciel.workflow.utils.TextUtils.toIso8601String;
import static java.lang.Integer.MAX_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.*;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.BooleanUtils.FALSE;
import static org.apache.commons.lang3.BooleanUtils.TRUE;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.http.HttpStatus.*;

@Log4j2
@RestController
@RequestMapping("instance")
@Tag(name = "instance", description = "Instance operations")
public class InstanceController {


    // <editor-fold desc="Beans">


    private final FlowableService flowableService;
    private final HistoryService historyService;
    private final RuntimeService runtimeService;
    private final DatabaseService databaseService;


    @Autowired
    public InstanceController(FlowableService flowableService,
                              RuntimeService runtimeService,
                              HistoryService historyService,
                              DatabaseService databaseService) {
        this.flowableService = flowableService;
        this.historyService = historyService;
        this.runtimeService = runtimeService;
        this.databaseService = databaseService;
    }


    // </editor-fold desc="Beans">


    @PostMapping
    @Operation(description = "Create new instance")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull Instance createInstance(@Parameter(name = "body")
                                            @RequestBody Instance instance) {

        log.info("createInstance tenantId:{} key:{} dueDate:{} creationW:{} validationW:{} variables:{}",
                instance.getTenantId(), instance.getDeploymentKey(), instance.getDueDate(), instance.getCreationWorkflowId(),
                instance.getValidationWorkflowId(), instance.getVariables()
        );

        if (isEmpty(instance.getValidationWorkflowId())) {
            throw new ResponseStatusException(BAD_REQUEST, "ValidationWorkflow needed");
        }

        if (instance.getVariables().keySet().stream().anyMatch(variable -> variable.startsWith(WORKFLOW_INTERNAL))) {
            throw new ResponseStatusException(BAD_REQUEST, "Instance variables starting with `" + WORKFLOW_INTERNAL + "` are reserved");
        }

        ProcessDefinition processDefinition = flowableService
                .getProcessDefinition(instance.getDeploymentKey(), null)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given deployment key is unknown"));

        HashMap<String, Object> variables = buildFlowableVariablesInternalMap(instance);
        variables.put(META_CREATION_DATE, toIso8601String(Date.from(Instant.now())));

        ProcessInstance processInstance = runtimeService
                .createProcessInstanceBuilder()
                .processDefinitionKey(processDefinition.getKey())
                .name(instance.getName())
                .businessKey(instance.getBusinessKey())
                .variables(variables)
                .overrideProcessDefinitionTenantId(instance.getTenantId())
                .start();

        instance.setId(processInstance.getId());
        instance.setStartTime(processInstance.getStartTime());

        return instance;
    }


    @NotNull private PaginatedList<Task> searchInstancesWithParams(String tenantId,
                                                                   int page,
                                                                   int pageSize,
                                                                   SortBy sortBy,
                                                                   boolean asc,
                                                                   String typeId,
                                                                   String subtypeId,
                                                                   String legacyId,
                                                                   String searchTerm,
                                                                   Long emitBeforeTime,
                                                                   Long stillSinceTime,
                                                                   State state,
                                                                   List<FilteringParameter> filteringParams) {

        List<FilteringParameter> mutableFilteringParams = new ArrayList<>(filteringParams);

        if (StringUtils.isNotEmpty(legacyId)) {
            mutableFilteringParams.add(new FilteringParameter(null, null, META_LEGACY_ID, legacyId));
        }

        if (mutableFilteringParams.isEmpty()) {
            mutableFilteringParams = null;
        }

        String decodedSearchTerm = Optional.ofNullable(searchTerm)
                .map(value -> URLDecoder.decode(value, UTF_8))
                .orElse(null);


        boolean useReducedQuery = mutableFilteringParams == null
                                  && state == null
                                  && stillSinceTime == null
                                  && StringUtils.isEmpty(decodedSearchTerm);

        List<Task> instances;
        if (useReducedQuery) {
            instances = databaseService.getTasksReduced(tenantId, typeId, subtypeId, emitBeforeTime, sortBy.name(), asc, page, pageSize);
        } else {
            instances = databaseService.getTasks(
                    tenantId,
                    mutableFilteringParams,
                    state,
                    sortBy.name(),
                    asc,
                    page,
                    pageSize,
                    typeId,
                    subtypeId,
                    decodedSearchTerm,
                    null,
                    emitBeforeTime,
                    stillSinceTime
            );
        }

        LongSupplier totalSupplier = () -> databaseService
                .countTasks(tenantId, filteringParams, state, typeId, subtypeId, searchTerm, null, emitBeforeTime, stillSinceTime);

        return new PaginatedList<>(instances, page, pageSize, totalSupplier);
    }


    @PostMapping("search")
    @Operation(description = "Get instance list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull PaginatedList<Task> searchInstances(@RequestParam(name = TENANT_ID_PARAM_NAME) String tenantId,
                                                        @RequestParam(required = false, defaultValue = "0") int page,
                                                        @RequestParam(required = false, defaultValue = "50") int pageSize,
                                                        @RequestParam(required = false, defaultValue = INSTANCE_NAME_VALUE) SortBy sortBy,
                                                        @RequestParam(required = false, defaultValue = TRUE) boolean asc,
                                                        @RequestBody InstanceSearchRequest request) {

        log.debug("searchInstances group:{} state:{}", request.groupIds(), request.state());

        List<FilteringParameter> filteringParams = Optional.ofNullable(request.groupIds())
                .stream()
                .flatMap(Collection::stream)
                .filter(StringUtils::isNotEmpty)
                .map(grpId -> new FilteringParameter(grpId, null, null, null))
                .toList();

        return searchInstancesWithParams(
                tenantId,
                page,
                pageSize,
                sortBy,
                asc,
                request.typeId(),
                request.subtypeId(),
                request.legacyId(),
                request.searchTerm(),
                request.emitBeforeTime(),
                request.stillSinceTime(),
                request.state(),
                filteringParams
        );

    }


    @GetMapping
    @Operation(description = "Get instance list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull PaginatedList<Task> getInstances(@RequestParam(name = TENANT_ID_PARAM_NAME) String tenantId,
                                                     @RequestParam(required = false, defaultValue = "0") int page,
                                                     @RequestParam(required = false, defaultValue = "50") int pageSize,
                                                     @RequestParam(required = false, defaultValue = INSTANCE_NAME_VALUE) SortBy sortBy,
                                                     @RequestParam(required = false, defaultValue = TRUE) boolean asc,
                                                     @RequestParam(required = false) String groupId,
                                                     @RequestParam(required = false) String typeId,
                                                     @RequestParam(required = false) String subtypeId,
                                                     @RequestParam(required = false) String legacyId,
                                                     @RequestParam(required = false) String searchTerm,
                                                     @RequestParam(required = false) Long emitBeforeTime,
                                                     @RequestParam(required = false) Long stillSinceTime,
                                                     @RequestParam(required = false) State state) {

        log.debug("getInstances group:{} state:{}", groupId, state);

        List<FilteringParameter> filteringParams = new ArrayList<>(Optional.ofNullable(groupId)
                .filter(StringUtils::isNotEmpty)
                .map(grpId -> new FilteringParameter(grpId, null, null, null))
                .map(Collections::singletonList)
                .orElse(emptyList()));

        return searchInstancesWithParams(
                tenantId,
                page,
                pageSize,
                sortBy,
                asc,
                typeId,
                subtypeId,
                legacyId,
                searchTerm,
                emitBeforeTime,
                stillSinceTime,
                state,
                filteringParams
        );
    }


    @PostMapping("list")
    @Operation(description = "Get specific instances for a maximum of 50 ids")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull List<Instance> getSpecificInstances(@RequestBody() List<String> idList,
                                                        @RequestParam(defaultValue = "false") boolean withHistory,
                                                        @RequestParam(required = false) String tenantId) {
        log.debug("getInstanceList idList:{}", idList);

        if (idList.size() > 50) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "La liste ne doit pas dépasser 50 ids.");
        }

        return idList.stream()
                .map(instanceId -> flowableService.getRuntimeInstance(instanceId, tenantId)
                        .or(() -> flowableService.getHistoricInstance(instanceId, tenantId))
                        .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown")))
                .map(fullInstance -> getInstance(fullInstance.getId(), fullInstance, withHistory, null))
                .toList();
    }


    @GetMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Get instance")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull Instance getInstance(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                         @PathVariable(name = Instance.API_PATH) String id,
                                         @InstanceResolved Instance instance,
                                         @RequestParam(defaultValue = FALSE) boolean withHistory,
                                         @RequestParam(name = TENANT_ID_PARAM_NAME, required = false) String tenantId) {

        log.debug("getInstance id:{} withHistory:{}", id, withHistory);
        log.debug("getInstance metadata:{}", instance.getVariables());
        log.debug("getInstance businessKey:{}", instance.getBusinessKey());

        // We have 3 types of tasks : The theoretical (future) ones, the historic (past) ones, and the pending (present) ones.
        // Fetching tasks one by one, sometimes we'll want to add a task to an existing list...
        // Building everything into 3 separate maps, and merging those at the end is way easier.
        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        AtomicBoolean inEndOfWorkflow = new AtomicBoolean(false);
        if (instance.getEndTime() == null) {

            List<Task> pendingTasks = flowableService.getPendingTasks(instance.getBusinessKey());
            inEndOfWorkflow.set(
                    pendingTasks.stream().anyMatch(task -> task.getExpectedAction() == ARCHIVE)
            );

            pendingTasks.forEach(t -> safeAdd(pendingTasksMap, Pair.of(t.getWorkflowIndex(), t.getStepIndex()), t));
        } else {
            inEndOfWorkflow.set(true);
        }

        if (withHistory) {

            // Retrieving history

            flowableService.getHistoryService()
                    .createHistoricTaskInstanceQuery()
                    .processInstanceBusinessKey(instance.getBusinessKey())
                    .includeProcessVariables()
                    .includeTaskLocalVariables()
                    .includeIdentityLinks()
                    .list()
                    .stream()
                    .map(Task::new)
                    // We don't want current tasks here, they'll be retrieved with appropriate permissions later
                    .filter(t -> t.getDate() != null)
                    .forEach(t -> safeAdd(historyTasksMap, Pair.of(t.getWorkflowIndex(), t.getStepIndex()), t));

            // Retrieving the original definition.
            // Useless if the process is already ended, thus every task will be in the historic ones.
            if (!inEndOfWorkflow.get()) {

                // Building up the trivial start and end steps definitions.
                safeAdd(definitionTasksMap,
                        Pair.of(0L, 0L), Task.builder()
                                .candidateGroups(singletonList(instance.getOriginGroup()))
                                .expectedAction(START)
                                .workflowIndex(0L)
                                .build()
                );

                long loopCount = Long.parseLong(instance.getVariables().getOrDefault(META_LOOP_COUNT, "0"));
                safeAdd(definitionTasksMap,
                        Pair.of(MAX_VALUE + loopCount, 0L), Task.builder()
                                .candidateGroups(singletonList(instance.getFinalGroup()))
                                .notifiedGroups(instance.getFinalNotifiedGroups())
                                .expectedAction(ARCHIVE)
                                .workflowIndex(MAX_VALUE + loopCount)
                                .build()
                );

                // If any task has been performed in an index > 1, then the creation workflow is already finished.
                // Everything is in history, the definition would be useless.
                boolean hasCreationWorkflow = StringUtils.isNotEmpty(instance.getCreationWorkflowId());
                boolean isNotInValidationAlready = historyTasksMap.keySet().stream()
                        .map(Pair::getLeft)
                        .filter(Objects::nonNull)
                        .noneMatch(i -> i > 1);

                if (hasCreationWorkflow && isNotInValidationAlready) {

                    Map<Pair<Long, Long>, List<Task>> creationWorkflowDefinition = flowableService
                            .getDefinitionTasks(instance.getCreationWorkflowId(), 1L, tenantId)
                            .entrySet().stream()
                            .peek(e -> e.getValue().stream().filter(Objects::nonNull).forEach(t -> t.setWorkflowIndex(1L)))
                            .collect(toMap(e -> Pair.of(1L, e.getKey()), Map.Entry::getValue));

                    definitionTasksMap.putAll(creationWorkflowDefinition);
                }

                // If any chain was performed, we'll only want the last workflow's definition.
                // Every other would have been finished, entirely in history too.
                long validationWorkflowIndex = 2L + loopCount;

                Map<Pair<Long, Long>, List<Task>> validationWorkflowDefinition = flowableService
                        .getDefinitionTasks(instance.getValidationWorkflowId(), validationWorkflowIndex, tenantId)
                        .entrySet().stream()
                        .peek(e -> e.getValue().stream().filter(Objects::nonNull).forEach(t -> t.setWorkflowIndex(validationWorkflowIndex)))
                        .collect(toMap(e -> Pair.of(validationWorkflowIndex, e.getKey()), Map.Entry::getValue));

                definitionTasksMap.putAll(validationWorkflowDefinition);
                definitionTasksMap.forEach((key, value) -> log.debug("      modelTask : {} {}", key, value));
            }
        }

        // Merge everything into one single map, build and send back the result
        instance.setTaskList(CollectionUtils.mergeTaskLists(definitionTasksMap, historyTasksMap, pendingTasksMap));

        // Manually populate missing info
        instance.setState(CollectionUtils.computeInstanceStateFromTaskList(instance.getTaskList()));
        instance.getTaskList().forEach(task -> {
            task.setInstanceId(instance.getId());
            task.setInstanceName(instance.getName());
        });

        log.debug("getInstance taskSize:{} result:{}", instance.getTaskList().size(), instance);
        return instance;
    }


    @PutMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Edit instance")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void editInstance(@Parameter(description = Instance.API_DOC_ID_VALUE)
                             @PathVariable(name = Instance.API_PATH) String id,
                             @InstanceResolved Instance instance,
                             @RequestParam(name = TENANT_ID_PARAM_NAME, required = false) String tenantId,
                             @RequestBody Instance request) {

        log.info("editInstance id:{} request:{}", instance.getId(), request);

        if (request.getVariables().keySet().stream().anyMatch(variable -> variable.startsWith(WORKFLOW_INTERNAL))) {
            log.info("editInstance - some metadata uses worflow's internal prefix, abort");
            throw new ResponseStatusException(BAD_REQUEST, "Instance variables starting with `" + WORKFLOW_INTERNAL + "` are reserved");
        }

        // Actual edit

        HashMap<String, Object> variables = buildFlowableVariablesInternalMap(request);

        Optional.ofNullable(request.getName())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(n -> runtimeService.setProcessInstanceName(id, n));

        runtimeService.setVariables(id, variables);
    }


    @DeleteMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Delete instance")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteInstance(@Parameter(description = Instance.API_DOC_ID_VALUE)
                               @PathVariable(name = Instance.API_PATH) String id) {

        log.info("deleteInstance id:{}", id);
        boolean hasDeletedSomething = false;

        // The instance id may be in any repository.
        // We have to trigger every delete.

        try {
            runtimeService.deleteProcessInstance(id, "Cleanup");
            hasDeletedSomething = true;
        } catch (FlowableObjectNotFoundException | NullPointerException e) {
            log.error("deleteInstance - error while deleting runtime process");
            log.debug(e.getMessage());
        }

        log.debug("deleted the runtime process instance");

        try {
            historyService.createHistoricProcessInstanceQuery().processInstanceId(id).deleteWithRelatedData();
            hasDeletedSomething = true;
        } catch (FlowableObjectNotFoundException | NullPointerException e) {
            log.error("deleteInstance - error while deleting historic instance");
            log.debug(e.getMessage());
        }

        // Integrity check

        if (!hasDeletedSomething) {
            throw new ResponseStatusException(NOT_FOUND, "Nothing to delete with this id");
        }
        log.info("deleteInstance success");
    }


    @GetMapping("{" + Instance.API_PATH + "}/readTasks")
    @Operation(description = "Get an un-filtered read task list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull List<Task> getReadTasks(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                            @PathVariable(name = Instance.API_PATH) String id) {
        log.debug("getReadTasks id:{}", id);

        String businessKey = flowableService.getHistoricInstance(id, null)
                .map(Instance::getBusinessKey)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown"));

        return flowableService.getHistoryService()
                .createHistoricTaskInstanceQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .includeIdentityLinks()
                .processInstanceBusinessKey(businessKey)
                .taskName("read")
                .list()
                .stream()
                .map(Task::new)
                .toList();
    }


    @GetMapping("{" + Instance.API_PATH + "}/historyTasks")
    @Operation(description = "Get an un-filtered flattened event list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull List<Task> getHistoryTasks(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                               @PathVariable(name = Instance.API_PATH) String id) {
        log.debug("historyTasks id:{}", id);

        String businessKey = flowableService.getHistoricInstance(id, null)
                .map(Instance::getBusinessKey)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown"));

        log.debug("historyTasks businessKey:{}", businessKey);
        List<Task> result = flowableService.getHistoricTasks(businessKey);

        // Cleanup

        result.stream()
                // Optional tasks should not inherit main metadata.
                .filter(task -> (task.getExpectedAction() == READ) || (task.getExpectedAction() == UNDO))
                .forEach(task -> task.setVariables(emptyMap()));

        // Sending back results

        log.debug("historyTasks result:");
        result.forEach(t -> log.debug("  - id:{} user:{} date:{} state:{}", t.getId(), t.getAssignee(), t.getDate(), t.getState()));
        return result;
    }


    private static HashMap<String, Object> buildFlowableVariablesInternalMap(Instance instance) {

        HashMap<String, Object> variables = new HashMap<>(instance.getVariables());

        if (StringUtils.isEmpty(instance.getOriginGroup())) {throw new ResponseStatusException(BAD_REQUEST, "Origin group needed");}
        if (StringUtils.isEmpty(instance.getFinalGroup())) {throw new ResponseStatusException(BAD_REQUEST, "Final group needed");}

        Optional.ofNullable(instance.getDueDate())
                .ifPresent(date -> variables.put(META_DUE_DATE, toIso8601String(date)));

        Optional.ofNullable(instance.getVisibility())
                .filter(visibility -> visibility != CONFIDENTIAL)
                .map(Enum::toString)
                .ifPresent(visibility -> variables.put(META_VISIBILITY, visibility));

        Optional.ofNullable(instance.getCreationWorkflowId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(workflowId -> variables.put(META_CREATION_WORKFLOW_ID, workflowId));

        Optional.ofNullable(instance.getLegacyId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(id -> variables.put(META_LEGACY_ID, id));

        Optional.ofNullable(instance.getOriginGroup())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(id -> variables.put(META_ORIGIN_GROUP_ID, id));

        Optional.ofNullable(instance.getFinalGroup())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(id -> variables.put(META_FINAL_GROUP_ID, id));

        Optional.ofNullable(instance.getFinalNotifiedGroups())
                .filter(org.apache.commons.collections4.CollectionUtils::isNotEmpty)
                .ifPresent(id -> variables.put(META_FINAL_NOTIFIED_GROUPS, id));

        Optional.ofNullable(instance.getLegacyId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(id -> variables.put(META_LEGACY_ID, id));

        Optional.ofNullable(instance.getValidationWorkflowId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(id -> variables.put(META_VALIDATION_WORKFLOW_ID, id));

        return variables;
    }


    @GetMapping("fromOldDefinitions/{" + Group.API_PATH + "}")
    @Operation(description = "List of current instances using old version of definition by groupId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull Page<Instance> getInstancesUsingOldWorkflowDefinitionsByGroupId(@RequestParam(name = TENANT_ID_PARAM_NAME) String tenantId,
                                                                                    @PathVariable(Group.API_PATH) String groupId,
                                                                                    @PageableDefault(size = 50)
                                                                                    @ParameterObject Pageable pageable) {

        List<WorkflowDefinition> oldWorkflowDefinitionList = getOldWorkflowDefinitions(tenantId);

        Set<String> workflowsIdsMatchingDeskId = oldWorkflowDefinitionList.stream()
                .filter(workflow -> workflow.getSteps()
                        .stream()
                        .flatMap(step -> step.getValidatingDeskIds().stream())
                        .anyMatch(validatingDeskId -> StringUtils.equals(validatingDeskId, groupId)))
                .map(WorkflowDefinition::getId)
                .collect(toSet());

        log.debug("workflowsIdsMatchingDeskId : {}", workflowsIdsMatchingDeskId);

        if (workflowsIdsMatchingDeskId.isEmpty()) {
            log.debug("No old version workflow matching deskId found");
            return new PageImpl<>(emptyList(), pageable, 0);
        }

        Page<Instance> matchingInstances = getMatchingCurrentInstances(tenantId, pageable, workflowsIdsMatchingDeskId);

        log.debug("instancesMatchingWorkflowsIds : {}", matchingInstances.stream().map(Instance::getName).collect(Collectors.toList()));

        return matchingInstances;

    }

    @NotNull private List<WorkflowDefinition> getOldWorkflowDefinitions(String tenantId) {
        List<WorkflowDefinition> oldWorkflowDefinitionList = new ArrayList<>();

        DefinitionController definitionController = new DefinitionController(flowableService, flowableService.getRepositoryService(), databaseService);
        int pageSize = ApiUtils.PAGE_SIZE_MAX;

        for (int pageNumber = 0;
             pageNumber <= (ApiUtils.DEFINITIONS_MAX_COUNT / ApiUtils.PAGE_SIZE_MAX)
             && pageSize == ApiUtils.PAGE_SIZE_MAX;
             pageNumber++) {

            PaginatedList<WorkflowDefinition> workflows = definitionController.getWorkflowDefinitions(
                    pageNumber, ApiUtils.PAGE_SIZE_MAX, tenantId, WorkflowDefinitionSortBy.NAME, true, "");

            pageSize = workflows.getData().size();

            workflows.getData().forEach(workflow ->
                    oldWorkflowDefinitionList.addAll(flowableService.getRepositoryService()
                            .createProcessDefinitionQuery()
                            .processDefinitionTenantId(tenantId)
                            .processDefinitionKey(workflow.getKey())
                            .processDefinitionVersionLowerThan(workflow.getVersion())
                            .listPage(0, ApiUtils.PAGE_SIZE_MAX)
                            .stream()
                            .map(WorkflowDefinition::new)
                            .toList())
            );
        }

        oldWorkflowDefinitionList.forEach(workflow -> workflow.setSteps(definitionController.getStepsForWorkflowDefinition(workflow.getId())));
        return oldWorkflowDefinitionList;
    }


    @NotNull private Page<Instance> getMatchingCurrentInstances(String tenantId, Pageable pageable, Set<String> matchingWorkflowsIds) {
        ProcessInstanceQuery processInstanceQuery = flowableService.getRuntimeService()
                .createProcessInstanceQuery()
                .processInstanceTenantId(tenantId)
                .includeProcessVariables()
                .processDefinitionIds(matchingWorkflowsIds);

        int count = (int) processInstanceQuery.count();
        List<String> matchingInstancesId = processInstanceQuery
                .listPage(0, count)
                .stream()
                .map(Execution::getRootProcessInstanceId)
                .toList();

        int maxReturn = pageable.getPageSize();
        if (matchingInstancesId.size() < maxReturn) {
            maxReturn = matchingInstancesId.size();
        }

        return new PageImpl<>(
                getSpecificInstances(matchingInstancesId.subList(0, maxReturn), false, tenantId),
                pageable,
                count
        );
    }


    @GetMapping("fromOldDefinitions/byMetadata/{metadataId}")
    @Operation(description = "List of current instances using old version of definition by metadataId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull Page<Instance> getInstancesUsingOldWorkflowDefinitionsByMetadataId(@RequestParam(name = TENANT_ID_PARAM_NAME) String tenantId,
                                                                                       @PathVariable(name = "metadataId") String metadataId,
                                                                                       @PageableDefault(size = 50)
                                                                                       @ParameterObject Pageable pageable) {

        List<WorkflowDefinition> oldWorkflowDefinitionList = getOldWorkflowDefinitions(tenantId);

        Set<String> workflowsIdsMatchingMetadataId = getWorkflowsIdsMatchingMetadataId(
                metadataId, oldWorkflowDefinitionList, StepDefinition::getMandatoryValidationMetadataIds);

        workflowsIdsMatchingMetadataId.addAll(getWorkflowsIdsMatchingMetadataId(
                metadataId, oldWorkflowDefinitionList, StepDefinition::getMandatoryRejectionMetadataIds));

        log.debug("workflowsIdsMatchingMetadataId : {}", workflowsIdsMatchingMetadataId);

        if (workflowsIdsMatchingMetadataId.isEmpty()) {
            log.debug("No old version workflow matching metadataId found");
            return new PageImpl<>(emptyList(), pageable, 0);
        }

        Page<Instance> matchingInstances = getMatchingCurrentInstances(tenantId, pageable, workflowsIdsMatchingMetadataId);

        log.debug("instancesMatchingWorkflowsIds : {}", matchingInstances.stream().map(Instance::getName).collect(Collectors.toList()));
        return matchingInstances;
    }


    private static @NotNull Set<String> getWorkflowsIdsMatchingMetadataId(String metadataId,
                                                                          List<WorkflowDefinition> workflowDefinitionList,
                                                                          WorkflowStepDefinitionAccessor accessor) {
        return workflowDefinitionList.stream()
                .filter(workflow -> workflow
                        .getSteps()
                        .stream()
                        .map(accessor::access)
                        .flatMap(Collection::stream)
                        .anyMatch(workflowMetadataId -> StringUtils.equals(workflowMetadataId, metadataId)))
                .map(WorkflowDefinition::getId)
                .collect(toSet());
    }


    @GetMapping("byMetadata")
    @Operation(description = "List of current instances using metadata value")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull Page<Task> getInstancesByMetadataValue(@RequestParam(name = TENANT_ID_PARAM_NAME) String tenantId,
                                                           @ParameterObject() SearchByMetadataRequest request,
                                                           @PageableDefault(size = 50)
                                                           @ParameterObject Pageable pageable) {

        List<Task> taskList = new ArrayList<>();
        int pageSize = ApiUtils.PAGE_SIZE_MAX;

        for (int pageNumber = 0;
             pageNumber <= (ApiUtils.INSTANCE_MAX_COUNT / ApiUtils.PAGE_SIZE_MAX) && pageSize == ApiUtils.PAGE_SIZE_MAX;
             pageNumber++) {

            FilteringParameter filter = new FilteringParameter(null, null, request.getMetadataKey(), request.getMetadataValue());
            List<FilteringParameter> filteringParameters = new ArrayList<>();
            filteringParameters.add(filter);

            List<Task> taskPageContent = databaseService.getTasks(
                    tenantId,
                    filteringParameters,
                    null,
                    SortBy.INSTANCE_NAME.name(),
                    true,
                    pageNumber,
                    ApiUtils.PAGE_SIZE_MAX,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            );

            pageSize = taskPageContent.size();
            taskList.addAll(taskPageContent);
        }

        return new PageImpl<>(taskList, pageable, taskList.size());
    }


}
