/*
 * Workflow
 * Copyright (C) 2019-2024 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.utils;


import lombok.Data;


public class ApiUtils {


    public static final String TENANT_ID_PARAM_NAME = "tenantId";

    public static final String CODE_200 = "200";
    public static final String CODE_201 = "201";
    public static final String CODE_204 = "204";
    public static final String CODE_400 = "400";
    public static final String CODE_404 = "404";
    public static final String CODE_406 = "406";
    public static final String CODE_407 = "407";
    public static final String CODE_409 = "409";
    public static final String CODE_507 = "507";


    public static class Group {

        public static final String API_DOC_ID_VALUE = "Group id";
        public static final String API_PATH = "groupId";

    }


    public static final int INSTANCE_MAX_COUNT = 10 * 1000 * 1000;
    public static final int DEFINITIONS_MAX_COUNT = 250000;
    public static final int PAGE_SIZE_MAX = 1000;


    /**
     * This is only used on error declaration.
     * TODO: Find the SpringBoot inner error message model instead.
     */
    @Data
    public static class ErrorResponse {

        private String timestamp;
        private int status;
        private String error;
        private String message;
        private String path;

    }


}

