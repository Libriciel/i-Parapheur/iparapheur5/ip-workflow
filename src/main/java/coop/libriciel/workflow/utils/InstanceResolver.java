/*
 * Workflow
 * Copyright (C) 2019-2024 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.utils;

import coop.libriciel.workflow.configuration.WebConfig;
import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.services.FlowableService;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.server.ResponseStatusException;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.workflow.utils.ApiUtils.TENANT_ID_PARAM_NAME;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.web.servlet.HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE;

/**
 * This is an utility class,
 * registering it in {@link WebConfig} will factorize the {@link Instance} retrieving in controller classes.
 */
@Log4j2
public class InstanceResolver implements HandlerMethodArgumentResolver {


    @Parameter(hidden = true)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface InstanceResolved {}


    // <editor-fold desc="Beans">


    private final FlowableService flowableService;


    public InstanceResolver(FlowableService flowableService) {
        this.flowableService = flowableService;
    }


    // </editor-fold desc="Beans">


    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(InstanceResolved.class) != null;
    }


    @Override
    public Instance resolveArgument(@NotNull MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer,
                                    @NotNull NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) {
        // noinspection unchecked
        Map<String, String> variables = (Map<String, String>) Optional.of(nativeWebRequest)
                .map(r -> r.getNativeRequest(HttpServletRequest.class))
                .map(r -> r.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE))
                .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Cannot read request"));

        Map<String, String[]> queryParams = Optional.of(nativeWebRequest)
                .map(r -> r.getNativeRequest(HttpServletRequest.class))
                .map(HttpServletRequest::getParameterMap)
                .orElse(new HashMap<>());

        String tenantId = Arrays.stream(queryParams.getOrDefault(TENANT_ID_PARAM_NAME, new String[0]))
                .findFirst()
                .orElse(null);

        String instanceId = variables.get(Instance.API_PATH);

        log.debug("InstanceResolver tenantId:{}, instanceId:{}", tenantId, instanceId);

        return flowableService.getRuntimeInstance(instanceId, tenantId)
                .or(() -> flowableService.getHistoricInstance(instanceId, tenantId))
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown"));
    }


}
