Change Log
==========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.11.6] - 2024-03-20
[1.11.6]: https://gitlab.libriciel.fr/outils/workflow/tags/1.11.6

### Fixed

- Libraries update


## [1.11.5] - 2024-03-15
[1.11.5]: https://gitlab.libriciel.fr/outils/workflow/tags/1.11.5

### Fixed

- Libraries update


## [1.11.4] - 2024-03-14
[1.11.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.11.4

### Added

- Renovate component

### Fixed

- Libraries update


## [1.11.3] - 2024-02-27
[1.11.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.11.3

### Fixed

- CI deployment


## [1.11.2] - 2024-02-27
[1.11.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.11.2

### Fixed

- Library generation on CI


## [1.11.1] - 2024-02-20
[1.11.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.11.1

### Added

- `includeInstanceData` parameter on `getTask`, to fetch the instance id/name
- Default name on definition creation/edition

### Fixed

- Computed `state` field on `getInstance` result


## [1.11.0] - 2024-02-16
[1.11.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.11.0

### Removed

- StepDefinition `id`
- StepDefinition `name`


## [1.10.3] - 2024-01-10
[1.10.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.10.3

### Fixed

- More logs


## [1.10.2] - 2023-11-20
[1.10.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.10.2

### Fixed

- Simpler SQL query on getInstance


## [1.10.1] - 2023-11-06
[1.10.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.10.1

### Added

- More info in the getTask


## [1.10.0] - 2023-10-13
[1.10.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.10.0

### Added

- Get instances by metadata existence
- Get definitions by metadata existence


## [1.9.2] - 2023-09-27
[1.9.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.9.2

### Added

- Get instance by old definition

### Fixed

- "desks" renamed as "groups"


## [1.9.1] - 2023-09-21
[1.9.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.9.1

### Added

- Get definition by desk existence
- List instances by ids


## [1.9.0] - 2023-08-04
[1.9.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.9.0

### Fixed

- The lowercase/uppercase State ambiguity has been set to uppercase-only
- Full folder edit request

### Removed

- Partial folder edit requests (creation/validation) workflow edit


## [1.8.1] - 2023-07-20
[1.8.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.8.1

### Added

- Optional `tenantId` parameter on every `GET`/`PUT` Instance, to narrow (optionally) the search

### Fixed

- Set a `tenantId` field on an Instance callback


## [1.8.0] - 2023-06-28
[1.8.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.8.0

### Fixed

- Use the workflow definition key on update
- Use the workflow definition key on delete

### Removed

- Workflow definition `deploymentId`, `suspended`, `category` unused fields


## [1.7.7] - 2023-06-20
[1.7.7]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.7

### Fixed

- Second opinion's comments and metadata


## [1.7.6] - 2023-06-15
[1.7.6]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.6

### Fixed

- Downstream tasks search, sorting, and filtering

## [1.7.5] - 2023-06-09
[1.7.5]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.5

### Fixed

- Definition refresh

### Removed

- Workflow definition `deploymentKey`


## [1.7.4] - 2023
[1.7.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.4

## [1.7.3] - 2023
[1.7.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.3

## [1.7.2] - 2023
[1.7.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.2

## [1.7.1] - 2023
[1.7.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.1

## [1.7.0] - 2023
[1.7.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.7.0

## [1.6.8] - 2022
[1.6.8]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.8

## [1.6.7] - 2022
[1.6.7]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.7

## [1.6.6] - 2022
[1.6.6]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.6

## [1.6.5] - 2022
[1.6.5]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.5

## [1.6.4] - 2022
[1.6.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.4

### Added

- Docker healthcheck
- Trivy monitoring


## [1.6.3] - 2022
[1.6.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.3


## [1.6.2] - 2022
[1.6.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.2


## [1.6.1] - 2022
[1.6.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.1


## [1.6.0] - 2022
[1.6.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.0


## [1.5.7] - 2022
[1.5.7]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.7


## [1.5.6] - 2022
[1.5.6]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.6


## [1.5.5] - 2022-07-04
[1.5.5]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.5

### Fixed

- External actor in history


## [1.5.4] - 2022-06-28
[1.5.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.4

### Added

- Start and final tasks wrapped as regular actions

### Fixed

- Undo
- Delegation in history


## [1.5.3] - 2022-06-20
[1.5.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.3

### Fixed

- Date filter


## [1.5.2] - 2022-06-13
[1.5.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.2

### Fixed

- Read state


## [1.5.1] - 2022-06-08
[1.5.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.1

### Fixed

- Visibility parse


## [1.5.0] - 2022-06-01
[1.5.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.0

### Added

- Second opinion's ASK dedicated action


## [1.4.2] - 2022-05-27
[1.4.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.4.2

### Fixed

- Minor crashes


## [1.4.1] - 2022-05-25
[1.4.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.4.1

### Added

- Read state in BPMN
- Prometheus monitoring entrypoint


## [1.4.0] - 2022-04-13
[1.4.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.4.0

### Fixed

- States
- Creation date


## [1.3.1] - 2022-04-01
[1.3.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.3.1

### Fixed

- Spring4Shell vulnerability


## [1.3.0] - 2022-03-30
[1.3.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.3.0

### Added

- Secure mail action


## [1.2.0] - 2022-03-26
[1.2.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.2.0

### Fixed

- Metadata
- Requests' sort-by


## [1.1.7] - 2022-03-09
[1.1.7]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.7

### Fixed

- End desk definition
- Parallel tasks retrieving


## [1.1.6] - 2022-03-07
[1.1.6]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.6

### Fixed

- Folder search
- Annotations in sub-tasks


## [1.1.5] - 2022-02-22
[1.1.5]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.5

### Fixed

- Info on get task


## [1.1.4] - 2022-02-11
[1.1.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.4

### Fixed

- Limit date


## [1.1.3] - 2022-02-04
[1.1.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.3

### Fixed

- Wrong parameter for delegations


## [1.1.2] - 2022-01-28
[1.1.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.2

### Fixed

- Sentry reporting


## [1.1.1] - 2022-01-20
[1.1.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.1

### Fixed

- Log4j2 patch


## [1.1.0] - 2022-01-10
[1.1.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.0

### Changed

- Swagger 2 to OAS3


## [0.2.0] - 2020-03-03
[0.2.0]: https://gitlab.libriciel.fr/outils/workflow/tags/0.2.0

### Added

- DueDate on instances


## [0.1.0] - 2020-02-12
[0.1.0]: https://gitlab.libriciel.fr/outils/workflow/tags/0.1.0

### Added

- iparapheur's folder
- iparapheur's visa
- iparapheur's sign
- iparapheur's second_opinion
- Variables
- JUnit tests
- Swagger
- CRUD on definitions
- CRUD on tasks
- CRUD on instances

