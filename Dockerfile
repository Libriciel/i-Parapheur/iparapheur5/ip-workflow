#
# Workflow
# Copyright (C) 2019-2024 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

FROM hubdocker.libriciel.fr/eclipse-temurin:17.0.11_9-jre-alpine@sha256:ad9223070abcf5716e98296a98c371368810deb36197b75f3a7b74815185c5e3

HEALTHCHECK --interval=5s --timeout=2s --retries=60 \
  CMD wget --quiet --tries=1 --proxy off -O- http://localhost:8080/workflow/actuator/health | grep UP || exit 1

# Open Containers Initiative parameters
ARG CURRENT_BUILD_VERSION=""
ARG CURRENT_BUILD_DATE=""
ARG CURRENT_BUILD_COMMIT_REVISION=""
# Non-standard and/or deprecated variables, that are still widely used.
# If it is already set in the FROM image, it has to be overridden.
MAINTAINER Libriciel SCOP
LABEL maintainer="Libriciel SCOP"
LABEL org.label-schema.name="ip-workflow"
LABEL org.label-schema.vendor="Libriciel SCOP"
LABEL org.label-schema.build-date="$CURRENT_BUILD_DATE"
LABEL org.label-schema.schema-version="$CURRENT_BUILD_VERSION"
# Open Containers Initiative's image specifications
LABEL org.opencontainers.image.created="$CURRENT_BUILD_DATE"
LABEL org.opencontainers.image.version="$CURRENT_BUILD_VERSION"
LABEL org.opencontainers.image.revision="$CURRENT_BUILD_COMMIT_REVISION"
LABEL org.opencontainers.image.vendor="Libriciel SCOP"
LABEL org.opencontainers.image.title="ip-workflow"
LABEL org.opencontainers.image.description="iparapheur workflow engine, a wrapper around Flowable"
LABEL org.opencontainers.image.authors="Libriciel SCOP"
LABEL org.opencontainers.image.licenses="GNU Affero GPL v3"

# The Docker source image already contains tzdata timezones by default.
# From the Alpine doc, we just have to link and reference it : https://wiki.alpinelinux.org/wiki/Setting_the_timezone
ENV TZ="Europe/Paris"
RUN cp /usr/share/zoneinfo/$TZ /etc/localtime
RUN echo $TZ > /etc/timezone

# The Docker source image already contains musl-locales by default.
ENV LANG="fr_FR.UTF-8"
ENV LANGUAGE="fr_FR:en_US:en"
ENV LC_ALL="fr_FR.UTF-8"

VOLUME /tmp

ADD build/libs/workflow-*.jar workflow.jar
ENV JAVA_OPTS=""

ENTRYPOINT [ "sh", "-c", "java -XX:+UseContainerSupport -XX:MaxRAMPercentage=75.0 $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /workflow.jar" ]
